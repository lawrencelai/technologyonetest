using Services;

namespace UnitTest
{
    public class TestConverter
    {
        [Fact]
        public void TestCase1()
        {
            var result = ConverterService.Convert("123.45");

           Assert.Equal("ONE HUNDRED AND TWENTY-THREE DOLLARS AND FOURTY-FIVE CENTS", result);
        }

        [Fact]
        public void TestCase2()
        {
            var result = ConverterService.Convert("13.11");

            Assert.Equal("THIRTEEN DOLLARS AND ELEVEN CENTS", result);
        }
    }
}