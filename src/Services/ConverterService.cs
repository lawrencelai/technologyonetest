﻿namespace Services
{
    public class ConverterService
    {
        public static string Convert(string message)
        {
            var cent = ConvertCent(message);
            var number = ConvertNumber(message);

            if (!string.IsNullOrEmpty(cent))
                number = number + "DOLLARS AND " + cent;
            else if (string.IsNullOrEmpty(cent))
                number += "DOLLARS";

            return number;
        }
        private static string ConvertNumber(string number)
        {
            return Converter(number.Split(".")[0]);
        }

        private static string ConvertCent(string number)
        {
            if (number.Split(".").Length == 1)
                return "";
            else
                return Converter(number.Split(".")[1]) + "CENTS";
        }

        private static string Converter(string number)
        {
            var convertedMessage = "";
            var processedData = number;
            var originalData = number;

            var is2ndNumberIs1 = false;
            if (number.Length >= 2)
                is2ndNumberIs1 = System.Convert.ToInt32(originalData.Substring(originalData.Length - 2).Substring(0, 1)) == 1;

            for (var i = 0; i < number.Length; i++)
            {
                var decimal3string = "";
                var decimal2string = "";

                if (processedData.Length == 3)
                    decimal3string = "HUNDRED AND ";
                else if (processedData.Length == 2)
                    decimal3string = "";

                int data;
                if (processedData.Length > 1)
                    data = System.Convert.ToInt32(processedData.Substring(0, 1));
                else
                    data = System.Convert.ToInt32(processedData);

                if ((processedData.Length == 4 ||
                    processedData.Length == 3 ||
                    processedData.Length == 1) &&
                    !is2ndNumberIs1)
                {
                    if (data == 1)
                        decimal3string = "ONE" + " " + decimal3string;
                    else if (data == 2)
                        decimal3string = "TWO" + " " + decimal3string;
                    else if (data == 3)
                        decimal3string = "THREE" + " " + decimal3string;
                    else if (data == 4)
                        decimal3string = "FOUR" + " " + decimal3string;
                    else if (data == 5)
                        decimal3string = "FIVE" + " " + decimal3string;
                    else if (data == 6)
                        decimal3string = "SIX" + " " + decimal3string;
                    else if (data == 7)
                        decimal3string = "SEVEN" + " " + decimal3string;
                    else if (data == 8)
                        decimal3string = "EIGHT" + " " + decimal3string;
                    else if (data == 9)
                        decimal3string = "NINE" + " " + decimal3string;

                    convertedMessage += decimal3string;
                }
                else if (processedData.Length == 2)
                {
                    if (data == 1)
                    {
                        var decimal2number = System.Convert.ToInt32(originalData.Substring(originalData.Length - 2));
                        if (decimal2number == 11)
                            decimal2string = "ELEVEN ";
                        else if (decimal2number == 12)
                            decimal2string = "TWELVE ";
                        else if (decimal2number == 13)
                            decimal2string = "THIRTEEN ";
                        else if (decimal2number == 14)
                            decimal2string = "FOURTEEN ";
                        else if (decimal2number == 15)
                            decimal2string = "FIFTHEEN ";
                        else if (decimal2number == 16)
                            decimal2string = "SIXTEEN ";
                        else if (decimal2number == 17)
                            decimal2string = "SEVENTEEN ";
                        else if (decimal2number == 18)
                            decimal2string = "EIGHTEEN ";
                        else if (decimal2number == 19)
                            decimal2string = "NINETEEN ";
                    }
                    else if (data == 2)
                        decimal2string = "TWENTY-" + decimal3string;
                    else if (data == 3)
                        decimal2string = "THIRTY-" + decimal3string;
                    else if (data == 4)
                        decimal2string = "FOURTY-" + decimal3string;
                    else if (data == 5)
                        decimal2string = "FIFTHY-" + decimal3string;
                    else if (data == 6)
                        decimal2string = "SIXTY-" + decimal3string;
                    else if (data == 7)
                        decimal2string = "SEVENTY-" + decimal3string;
                    else if (data == 8)
                        decimal2string = "EIGHTTY-" + decimal3string;
                    else if (data == 9)
                        decimal2string = "NIGHTTY-" + decimal3string;

                    convertedMessage += decimal2string;
                }

                processedData = processedData[1..];
            }

            return convertedMessage;
        }
    }

}
